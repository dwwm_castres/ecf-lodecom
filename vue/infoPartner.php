<!DOCTYPE html>
<html lang="fr-FR">

<!-- Auteur : Jean-Brice GALLAND | Alain NIZARD | Jeremie BASTARDO| charlotte VIAL| martine SOULA-->
<!-- Modification : Cindy et Cyril-->

    <head>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

        <!-- CSS -->
        <link rel="stylesheet" href="css/style.css">

        <title>infoPartner</title>

    </head>

    <body>

        <!-- Contenu -->
        <div id="app" class="container-fluid" style="margin-top: 100px;">
            <div class="row">
                <div class="col"></div>
                <div class="col-xl-8">
                    <div class="card align-items-center">
                        <!-- Menu fixé au top -->
                        <div class="row fixed-top">
                            <a href="switch.php?allPartner=set" class="btn btn-dark col-6 pb-2" style="border-radius: 0 !important;">Listes des partenaires</a>
                            <a :href="'switch.php?allStructure=set&client_id='+partenaire.client_id" class="btn btn-dark col-6 pb-2" style="border-radius: 0 !important;">Gérer les clubs</a>
                        </div>

                        <div class="col-xl-12" style="margin-bottom: 20px">
                            <div class="text-center">
                                <a :href="partenaire.url">
                                    <img class="card-img-top" :src="partenaire.logo_url" alt="logo">
                                </a>
                            </div>
                        </div>

                        <div class="text-center">
                            <a href="#" class="card-link" data-toggle="tooltip" data-placement="bottom" title="Client_id">{{ partenaire.client_id }} </a>
                            <a href="#" class="card-link" data-toggle="tooltip" data-placement="bottom" title="Client_name">{{ partenaire.client_name }}</a>
                        </div>

                        <div class="col-md-12 col-xl-12" style="margin-top: 10px;">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-5">
                                        <h4 for="url">Site web </h4>
                                        <h4 for="dpo">DPO </h4>
                                        <h4 for="technical_contact">Contact technique </h4>
                                        <h4 for="commercial_contact">Contact commercial </h4>
                                    </div>

                                    <div class="col-md-7">
                                        <p type="text" id="url" name="url" class="card-text"> {{ partenaire.url }}</p>
                                        <p type="text" id="url" name="dpo" class="card-text"> {{ partenaire.dpo }}</p>
                                        <p type="text" id="url" name="technical_contact" class="card-text"> {{ partenaire.technical_contact }}</p>
                                        <p type="text" id="url" name="commercial_contact" class="card-text"> {{ partenaire.commercial_contact }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4 for="full_description" style="text-align:center;">FULL DESCRIPTION</h4>
                        <div class="col-md-12 col-xl-12">
                            <p type="text" id="full_description" name="full_description" class="card-text"> {{ partenaire.full_description }}</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm"></div>
            </div>

            <!-- <div class="row justify-content-around">-->
            <div class="row justify-content-end" style="margin-right: 350px; margin-top: 50px">
                <form id="droit" method="POST" :action="'switch.php?infoPartner=set&client_id='+partenaire.client_id">
                    <div class="custom-control custom-switch" v-if="partenaire.active == 'Y'">
                        <input v-on:click="confirmation_p('desactiver',$event)" type="checkbox" class="custom-control-input" id="customSwitchY" checked>
                        <input type="hidden" name="activer" id="activer" value="N">
                        <label class="custom-control-label" for="customSwitchY">Active</label>
                    </div>

                    <div class="custom-control custom-switch" v-else>
                        <input v-on:click="confirmation_p('activer',$event)" type="checkbox" class="custom-control-input" id="customSwitchN">
                        <input type="hidden" name="activer" id="activer" value="Y">
                        <label class="custom-control-label" for="customSwitchN">Inactive</label>
                    </div>
                </form>
            </div>
        </div>

        <!-- Vue js -->
        <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12"></script>

        <!-- js -->
        <script>
            const donnees = <?php echo json_encode($data); ?>;
        
            const app = new Vue({
                el: "#app",
                data: function() {
                    return donnees;
                },

                methods: {
                    confirmation_p: function(message, event) {
                        result = confirm("etes vous sur de vouloir " + message);
                        if (result) {
                            document.forms["droit"].submit();
                        } else {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                    }
                }
            })
        </script>
    
    </body>

</html>